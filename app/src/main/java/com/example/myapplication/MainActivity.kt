package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.animation.AnimationUtils
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val ttb=AnimationUtils.loadAnimation(this, R.anim.ttb)
        val for_log_in=AnimationUtils.loadAnimation(this, R.anim.for_log_in)
        val for_sign_up=AnimationUtils.loadAnimation(this, R.anim.for_sign_up)

        email.startAnimation(ttb)
        password.startAnimation(ttb)
        singup_button.startAnimation(for_sign_up)
        login_button.startAnimation(for_log_in)
    }
}